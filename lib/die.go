package lib

import (
	"fmt"
	"os"
)

func Die(err error) {
	_, _ = fmt.Fprintf(os.Stderr, "ERROR: %+v\n", err)
	os.Exit(1)
}
