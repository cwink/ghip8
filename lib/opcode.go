package lib

import (
	"fmt"
)

type OpCode uint16

func (oc OpCode) A() uint8 {
	return uint8((oc >> 12) & 0xF)
}

func (oc OpCode) Disasm() (string, error) {
	switch oc.A() {
	case 0x0:
		switch oc.KK() {
		case 0xE0:
			return SymbolCLS, nil
		case 0xEE:
			return SymbolRET, nil
		default:
			return fmt.Sprintf("%-5s %d", SymbolSYS, oc.NNN()), nil
		}
	case 0x1:
		return fmt.Sprintf("%-5s %d", SymbolJP, oc.NNN()), nil
	case 0x2:
		return fmt.Sprintf("%-5s %d", SymbolCALL, oc.NNN()), nil
	case 0x3:
		return fmt.Sprintf("%-5s %s%d, %d", SymbolSE, SymbolV, oc.X(), oc.KK()), nil
	case 0x4:
		return fmt.Sprintf("%-5s %s%d, %d", SymbolSNE, SymbolV, oc.X(), oc.KK()), nil
	case 0x5:
		if oc.N() == 0x0 {
			return fmt.Sprintf("%-5s %s%d, %s%d", SymbolSE, SymbolV, oc.X(), SymbolV, oc.Y()), nil
		}
	case 0x6:
		return fmt.Sprintf("%-5s %s%d, %d", SymbolLD, SymbolV, oc.X(), oc.KK()), nil
	case 0x7:
		return fmt.Sprintf("%-5s %s%d, %d", SymbolADD, SymbolV, oc.X(), oc.KK()), nil
	case 0x8:
		switch oc.N() {
		case 0x0:
			return fmt.Sprintf("%-5s %s%d, %s%d", SymbolLD, SymbolV, oc.X(), SymbolV, oc.Y()), nil
		case 0x1:
			return fmt.Sprintf("%-5s %s%d, %s%d", SymbolOR, SymbolV, oc.X(), SymbolV, oc.Y()), nil
		case 0x2:
			return fmt.Sprintf("%-5s %s%d, %s%d", SymbolAND, SymbolV, oc.X(), SymbolV, oc.Y()), nil
		case 0x3:
			return fmt.Sprintf("%-5s %s%d, %s%d", SymbolXOR, SymbolV, oc.X(), SymbolV, oc.Y()), nil
		case 0x4:
			return fmt.Sprintf("%-5s %s%d, %s%d", SymbolADD, SymbolV, oc.X(), SymbolV, oc.Y()), nil
		case 0x5:
			return fmt.Sprintf("%-5s %s%d, %s%d", SymbolSUB, SymbolV, oc.X(), SymbolV, oc.Y()), nil
		case 0x6:
			return fmt.Sprintf("%-5s %s%d", SymbolSHR, SymbolV, oc.X()), nil
		case 0x7:
			return fmt.Sprintf("%-5s %s%d, %s%d", SymbolSUBN, SymbolV, oc.X(), SymbolV, oc.Y()), nil
		case 0xE:
			return fmt.Sprintf("%-5s %s%d", SymbolSHL, SymbolV, oc.X()), nil
		}
	case 0x9:
		if oc.N() == 0x0 {
			return fmt.Sprintf("%-5s %s%d, %s%d", SymbolSNE, SymbolV, oc.X(), SymbolV, oc.Y()), nil
		}
	case 0xA:
		return fmt.Sprintf("%-5s %s, %d", SymbolLD, SymbolI, oc.NNN()), nil
	case 0xB:
		return fmt.Sprintf("%-5s %s0, %d", SymbolJP, SymbolV, oc.NNN()), nil
	case 0xC:
		return fmt.Sprintf("%-5s %s%d, %d", SymbolRND, SymbolV, oc.X(), oc.KK()), nil
	case 0xD:
		return fmt.Sprintf("%-5s %s%d, %s%d, %d", SymbolDRW, SymbolV, oc.X(), SymbolV, oc.Y(), oc.N()), nil
	case 0xE:
		switch oc.KK() {
		case 0x9E:
			return fmt.Sprintf("%-5s %s%d", SymbolSKP, SymbolV, oc.X()), nil
		case 0xA1:
			return fmt.Sprintf("%-5s %s%d", SymbolSKNP, SymbolV, oc.X()), nil
		}
	case 0xF:
		switch oc.KK() {
		case 0x07:
			return fmt.Sprintf("%-5s %s%d, %s", SymbolLD, SymbolV, oc.X(), SymbolDT), nil
		case 0x0A:
			return fmt.Sprintf("%-5s %s%d, %s", SymbolLD, SymbolV, oc.X(), SymbolK), nil
		case 0x15:
			return fmt.Sprintf("%-5s %s, %s%d", SymbolLD, SymbolDT, SymbolV, oc.X()), nil
		case 0x18:
			return fmt.Sprintf("%-5s %s, %s%d", SymbolLD, SymbolST, SymbolV, oc.X()), nil
		case 0x1E:
			return fmt.Sprintf("%-5s %s, %s%d", SymbolADD, SymbolI, SymbolV, oc.X()), nil
		case 0x29:
			return fmt.Sprintf("%-5s %s, %s%d", SymbolLD, SymbolF, SymbolV, oc.X()), nil
		case 0x33:
			return fmt.Sprintf("%-5s %s, %s%d", SymbolLD, SymbolB, SymbolV, oc.X()), nil
		case 0x55:
			return fmt.Sprintf("%-5s %s, %s%d", SymbolLD, SymbolIM, SymbolV, oc.X()), nil
		case 0x65:
			return fmt.Sprintf("%-5s %s%d, %s", SymbolLD, SymbolV, oc.X(), SymbolIM), nil
		}
	}
	return "", fmt.Errorf("invalid opcode 0x%4.4X encountered", oc)
}

func (oc OpCode) KK() uint8 {
	return uint8(oc & 0xFF)
}

func (oc OpCode) N() uint8 {
	return uint8(oc & 0xF)
}

func (oc OpCode) NNN() uint16 {
	return uint16(oc & 0xFFF)
}

func (oc OpCode) X() uint8 {
	return uint8((oc >> 8) & 0xF)
}

func (oc OpCode) Y() uint8 {
	return uint8((oc >> 4) & 0xF)
}
