package lib

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"os"
)

type ocReader struct {
	fil *os.File
	oc  OpCode
	dn  bool
}

type OcReader interface {
	Close()
	Next() (more bool, err error)
	OpCode() OpCode
}

func (ocr *ocReader) Close() {
	ocr.fil.Close()
}

func (ocr *ocReader) Next() (more bool, err error) {
	if ocr.dn {
		return false, nil
	}
	if err := binary.Read(ocr.fil, binary.BigEndian, &ocr.oc); err != nil {
		if errors.Is(err, io.EOF) {
			return false, fmt.Errorf("failed to read from file: %w", err)
		}
		ocr.dn = true
		return false, nil
	}
	return true, nil
}

func (ocr *ocReader) OpCode() OpCode {
	return ocr.oc
}

func NewOcReader(fname string) (OcReader, error) {
	ocr := &ocReader{
		oc: 0x0,
		dn: false,
	}
	var err error
	ocr.fil, err = os.Open(fname)
	if err != nil {
		return nil, fmt.Errorf("failed to open file: %w", err)
	}
	return ocr, nil
}
