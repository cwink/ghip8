package main

import (
	"fmt"
	"gitlab.com/cwink/ghip8/lib"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: ./disasm [rom]\n\n")
		fmt.Printf("\trom - The rom file to disassemble.\n")
		os.Exit(0)
	}
	ocr, err := lib.NewOcReader(os.Args[1])
	if err != nil {
		lib.Die(err)
	}
	defer ocr.Close()
	for {
		mo, err := ocr.Next()
		if err != nil {
			lib.Die(err)
		}
		if !mo {
			break
		}
		v, err := ocr.OpCode().Disasm()
		if err != nil {
			lib.Die(err)
		}
		fmt.Printf("%s\n", v)
	}
}
